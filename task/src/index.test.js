import {validBitString , removeRedundantBits , countBitInARow , decodeBits , decodeMorse} from './index';

//------------- valid bit string --------------------------------------
test("valid bit input", () => {
    expect(validBitString("1100110000011")).toBe(true);
})

test("invalid bit input", () => {
    for( let char = "2"; char <= "9"; char ++){
        expect(validBitString(String.fromCharCode(char))).toBe(false)
    }
})
//----------------------------------------------------------------------

//------------- remove redundant  --------------------------------------

test("remove redundant 0 bits", () => {
    expect(removeRedundantBits("000111000")).toBe("111");
    expect(removeRedundantBits("111000")).toBe("111");
    expect(removeRedundantBits("000111")).toBe("111");
    expect(removeRedundantBits("00000000")).toBe("0");
})

//----------------------------------------------------------------------

//------------- count same bit in a row  -------------------------------
test("count the number of same bit in a row", () => {
    expect(countBitInARow("000111000","0")).toBe(3);
    expect(countBitInARow("000111000","1")).toBe(0);
})
//----------------------------------------------------------------------

//------------- decode bit string to morse  ----------------------------
test("decode a bit string to morse", () => {
    expect(decodeBits("1100110011001100000011000000111111001100111111001111110000000000000011001111110011111100111111000000110011001111110000001111110011001100000011")).toBe(".... . -.--   .--- ..- -.. .")
})
//----------------------------------------------------------------------

//------------- decode morse string to message  ------------------------
test("decode a morse string to message", () => {
    expect(decodeMorse(".... . -.--   .--- ..- -.. .")).toBe("HEY JUDE")
})
//----------------------------------------------------------------------