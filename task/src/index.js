
/**
 * checks if the bit string consists of only 1s and 0s
 * @param {*} bits a string of bits
 * @returns true if the bits are valid and false eitherwise 
 */
const validBitString = (bits) => {
    return bits.split('').every((bit)=>bit === '0' || bit === '1')
}

/**
 * removes extra 0 bits from the start and the end of the string 
 * @param {*} bits a string of bits
 * @returns new string of bits
 */
const removeRedundantBits = (bits) => {
    let start = 0;
    let end = bits.length;
    for(let index = 0; index < bits.length; index++){
        if(bits.charAt(index) == '0'){
            start++;
        }else{
            break;
        }
    }
    for(let index = bits.length - 1; index >= 0; index--){
        if(bits.charAt(index) == '0'){
            end--;
        }else{
            break;
        }
    }
    if(end < 0 || start === bits.length){
        return "0";
    }
    return bits.slice(start,end);
}

/**
 * count the number of times the bit to count appears in a row
 * @param {*} bits a string of bits
 * @param {*} bitToCount the bit we want to count appearances in a row
 * @returns the number of appearances in a row
 */
const countBitInARow = (bits,bitToCount) => {
    let count = 0;
    for(let currentBitIndex = 0; currentBitIndex < bits.length; currentBitIndex++){
        if(bits.charAt(currentBitIndex) === bitToCount){
            count++
        }else{
            break;
        }
    }
    return count;
}
const numOf1BitsDash = 6;
const spaceBetweenWords = 14; 
const spaceBetweenChars = 6;
/**
 * translates a bit string to morse string
 * @param {*} bits a string of bits
 * @returns decoded morse string
 */
const decodeBits = (bits) => {
    let morseString = "";
    if(validBitString(bits)){
        bits = removeRedundantBits(bits);
        if(bits !== "0"){
            while(bits.length > 0){
                let currentBit = bits.charAt(0);
                let numBitInARow = countBitInARow(bits,currentBit);
                if(currentBit === "1"){
                    if(numBitInARow >= numOf1BitsDash){
                        morseString += "-";
                    }else{
                        morseString += ".";
                    }
                }else{
                    if(numBitInARow === spaceBetweenWords){
                        //3 spaces
                        morseString +="   ";
                    }else if(numBitInARow === spaceBetweenChars){
                        //1 space
                        morseString +=" ";
                    }
                }
                bits = bits.slice(numBitInARow);
            }
            return morseString;
        }else{
            return "";
        }
    }
    return "";
}

// A MORSE DICTIONARY 
const morseMap = {".-":"A" , "-...":"B" , "-.-.":"C" , "-..":"D" , ".":"E" , "..-.":"F" , "--.":"G" , "....":"H" , "..":"I" , ".---":"J" , "-.-":"K" , ".-..":"L" , "--":"M" , "-.":"N" , "---":"O" , ".--.":"P" , "--.-":"Q" , ".-.":"R" , "...":"S" , "-":"T" , "..-":"U" , "...-":"V" , ".--":"W" , "-..-":"X" , "-.--":"Y" , "--..":"Z"}

/**
 * translates the morse string to the message
 * @param {*} morseCode a string of morse
 * @returns the message
 */
const decodeMorse = (morseCode) => {
    let message = "";
    let currentChar = "";
    for(let currentMorseChar = 0; currentMorseChar < morseCode.length; currentMorseChar++){
        if(morseCode.charAt(currentMorseChar) === " "){
            message += morseMap[currentChar];
            currentChar = "";
            if(morseCode.charAt(currentMorseChar+1) === " "){
                // there are 3 spaces so need to move current morse char by 3 and it is moved 1 time by the for loop
                message += " ";
                currentMorseChar += 2;
            }
        }else{
            currentChar += morseCode.charAt(currentMorseChar);
        }
    }
    if(currentChar !== ""){
        message += morseMap[currentChar];
    }
    return message;
}

export{ validBitString , removeRedundantBits , countBitInARow ,decodeBits ,decodeMorse};